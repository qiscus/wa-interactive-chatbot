import requests
from app.config import Config


def send_text_message(
    recipient: str,
    text: str,
    preview_url: bool = True,
    recipient_type: str = "individual",
):
    res = requests.post(
        Config.get_outbound_url(),
        headers={
            "Qiscus-App-Id": Config.qiscus_app_id,
            "Qiscus-Secret-Key": Config.qiscus_secret_key,
        },
        json={
            "preview_url": preview_url,
            "recipient_type": recipient_type,
            "to": recipient,
            "type": "text",
            "text": {"body": text},
        },
    )

    print(res.text)

    return True


def send_interactive_message(
    recipient: str,
    interactive_type: str,
    body: dict,
    action: dict,
    header: dict = None,
    footer: dict = None,
    recipient_type: str = "individual",
):
    body = {
        "recipient_type": recipient_type,
        "to": recipient,
        "type": "interactive",
        "interactive": {"type": interactive_type, "body": body, "action": action},
    }

    # OPTIONAL
    if header is not None:
        body["interactive"]["header"] = header

    if footer is not None:
        body["interactive"]["footer"] = footer

    res = requests.post(
        Config.get_outbound_url(),
        headers={
            "Qiscus-App-Id": Config.qiscus_app_id,
            "Qiscus-Secret-Key": Config.qiscus_secret_key,
        },
        json=body,
    )

    print(res.text)

    return True


def send_main_menu(recipient: str):
    send_interactive_message(
        recipient=recipient,
        interactive_type="list",
        header={
            "type": "text",
            "text": "Menu Utama",
        },
        body={
            "text": "Silakan pilih salah satu produk yang ingin Anda akses",
        },
        # footer={"text": "powered by Qiscus"},
        action={
            "button": "Daftar Produk",
            "sections": [
                {
                    "rows": [
                        {
                            "id": "mc",
                            "title": "Multichannel Chat",
                            "description": "Sentralisasi konsumen Anda ke satu dashboard",
                        },
                        {
                            "id": "chat-sdk",
                            "title": "In-App Chat SDK",
                            "description": "Ciptakan fitur obrolan di aplikasi Anda",
                        },
                        {
                            "id": "meet-sdk",
                            "title": "Meet SDK",
                            "description": "Hubungkan pengguna dengan audio dan video call",
                        },
                    ],
                }
            ],
        },
    )


def send_mc_menu(recipient: str):
    send_interactive_message(
        recipient=recipient,
        interactive_type="button",
        header={
            "type": "text",
            "text": "Multichannel",
        },
        body={
            "text": "Sentralisasikan seluruh pesan konsumen dari aplikasi Anda dan aplikasi lainnya ke dalam satu dashboard.\n\nPilih akses yang Anda inginkan"
        },
        action={
            "buttons": [
                {
                    "type": "reply",
                    "reply": {"id": "mc-register", "title": "🔥 Registrasi"},
                },
                {
                    "type": "reply",
                    "reply": {"id": "mc-login", "title": "❤️ Login"},
                },
            ]
        },
    )


def send_mc_register_link(recipient: str):
    text = "Anda dapat melakukan registrasi Multichannel menggunakan tautan di bawah ini\n\nhttps://www.qiscus.com/multichannel/register"

    return send_text_message(recipient, text)


def send_mc_login_link(recipient: str):
    text = "Anda dapat login ke Multichannel menggunakan tautan di bawah ini\n\nhttps://multichannel.qiscus.com/"

    return send_text_message(recipient, text)


def send_chat_sdk_menu(recipient: str):
    send_interactive_message(
        recipient=recipient,
        interactive_type="button",
        header={
            "type": "text",
            "text": "In-App Chat SDK",
        },
        body={
            "text": "Ciptakan fitur obrolan yang sangat di aplikasi Anda dengan Qiscus In-App Chat SDK & Messaging API.\n\nPilih akses yang Anda inginkan"
        },
        action={
            "buttons": [
                {
                    "type": "reply",
                    "reply": {
                        "id": "chat-sdk-register",
                        "title": "🔥 Registrasi",
                    },
                },
                {
                    "type": "reply",
                    "reply": {"id": "chat-sdk-login", "title": "❤️ Login"},
                },
            ]
        },
    )

def send_chat_sdk_register_link(recipient: str):
    text = "Silakan hubungi kami untuk melakukan registrasi akun In-App Chat SDK\n\nhttps://www.qiscus.com/id/contact"

    return send_text_message(recipient, text)


def send_chat_sdk_login_link(recipient: str):
    text = "Anda dapat login ke dashboard In-App Chat SDK menggunakan tautan di bawah ini\n\nhttps://www.qiscus.com/dashboard/login"

    return send_text_message(recipient, text)

def send_meet_sdk_menu(recipient: str):
    send_interactive_message(
        recipient=recipient,
        interactive_type="button",
        header={
            "type": "text",
            "text": "Meet SDK",
        },
        body={
            "text": "Hubungkan pengguna dengan audio dan video call melalui aplikasi video konferensi bisnis Anda.\n\nPilih akses yang Anda inginkan"
        },
        action={
            "buttons": [
                {
                    "type": "reply",
                    "reply": {"id": "meet-sdk-register", "title": "🔥 Registrasi"},
                },
                {
                    "type": "reply",
                    "reply": {"id": "meet-sdk-login", "title": "❤️ Login"},
                },
            ]
        },
    )

def send_meet_sdk_register_link(recipient: str):
    text = "Silakan hubungi akun Whatsapp kami untuk melakukan registrasi akun Meet SDK\n\nhttps://wa.me/6281228430523&text=Hi%20Qiscus,%20saya%20ingin%20mengetahui%20lebih%20lanjut%20tentang%20Qiscus%20Meet%20SDK"

    return send_text_message(recipient, text)


def send_meet_sdk_login_link(recipient: str):
    text = "Silakan hubungi akun support kami untuk mendapatkan akses ke dashboard Meet SDK\n\nhttps://support.qiscus.com/hc/en-us/requests/new"

    return send_text_message(recipient, text)
