from typing import Optional
from pydantic import BaseSettings


class NewConfig(BaseSettings):
    wa_channel_id: int
    qiscus_app_id: str
    qiscus_secret_key: str
    qiscus_multichannel_base_url: Optional[str] = "https://multichannel.qiscus.com"

    def get_outbound_url(self):
        return f"{self.qiscus_multichannel_base_url}/whatsapp/v1/{self.qiscus_app_id}/{self.wa_channel_id}/messages"

    class Config:
        env_file = ".env"


Config = NewConfig()
