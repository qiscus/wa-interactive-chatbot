from fastapi import FastAPI
from app import whatsapp

bot = FastAPI()

@bot.get("/")
def root():
    return "ok"

@bot.post("/")
def inbound_messages(body: dict):
    try:
        print(body)
        print("\n\n")

        contacts = body["contacts"]
        if len(contacts) < 1:
            return "ok"

        messages = body["messages"]
        if len(messages) < 1:
            return "ok"

        contact = contacts[0]
        recipient = contact["wa_id"]

        message = messages[0]
        message_type = message["type"]

        if message_type != "interactive" and message_type != "text":
            return "ok"

        if message_type == "text":
            text = message["text"]["body"]
            if "menu" in text.lower():
                whatsapp.send_main_menu(recipient)

            return "ok"

        interactive = message["interactive"]

        interactive_type = interactive["type"]

        object_id = interactive[interactive_type]["id"]

        if object_id == "mc":
            whatsapp.send_mc_menu(recipient)

        if object_id == "chat-sdk":
            whatsapp.send_chat_sdk_menu(recipient)

        if object_id == "meet-sdk":
            whatsapp.send_meet_sdk_menu(recipient)

        if object_id == "mc-register":
            whatsapp.send_mc_register_link(recipient)

        if object_id == "mc-login":
            whatsapp.send_mc_login_link(recipient)

        if object_id == "chat-sdk-register":
            whatsapp.send_chat_sdk_register_link(recipient)

        if object_id == "chat-sdk-login":
            whatsapp.send_chat_sdk_login_link(recipient)

        if object_id == "meet-sdk-register":
            whatsapp.send_meet_sdk_register_link(recipient)

        if object_id == "meet-sdk-login":
            whatsapp.send_meet_sdk_login_link(recipient)

        return "ok"
    except Exception as e:
        print(str(e))
        return f"Error: {str(e)}"
