# Whatsapp Interactive Chatbot

Communicate with chatbot using interactive messages in Whatsapp

### Requirements

- Python 3.8
- Qiscus Multichannel Account that has been integrated to Whatsapp channel

### Local setup

**Clone respository**

```bash
git clone git@bitbucket.org:qiscus/btpn-bulk-resolved-and-assign.git
cd btpn-bulk-resolved-and-assign
```

**Install packages**

```bash
pip install -r requirements.txt
```

**Create environment variables & replace the values with your own**

```bash
cp .env.sample .env
```

**Run the server**

```bash
uvicorn app.main:bot --reload
```

### Consuming webhook

This codebase will consume webhook from Qiscus Whatsapp Webhook, for local development, you need Ngrok (or other similar apps) to your Localhost. To install and run ngrok, please read its [official documentation](https://ngrok.com/docs).

After exposing your localhost, register your Ngrok (`https://*.ngrok.io`) endpoint to Qiscus by using this curl

```bash
curl --location --request POST 'https://multichannel.qiscus.com/whatsapp/{qiscus-app-id}/{wa-channel-id}/settings' \
--header 'Qiscus-App-Id: {qiscus-app-id}' \
--header 'Qiscus-Secret-Key: {qiscus-secret-key}' \
--header 'Content-Type: application/json' \
--data-raw '{
    "webhooks": {
        "url": "{ngrok-url}"
    }
}'
```
